package com.mycompany.triangleproblem;

import java.util.Optional;

@SuppressWarnings("WeakerAccess")
public class Triangle {
    Triangle() {
    }

    private Triangle(String[] coordinatesArray, String coordinates) {
        this.area = calculateArea(coordinatesArray);
        this.coordinates = coordinates;
    }

    private static final int VALID_COORDINATE_QUANTITY = 6;
    private String coordinates;
    private float area;

    public static Optional<Triangle> parse(String inputCoordinates) {
        if (inputCoordinates.matches("^[-0-9 ]*$")) {
            String[] coordinatesArray = inputCoordinates.split(" ");
            if (coordinatesArray.length == VALID_COORDINATE_QUANTITY) {
                Triangle triangle = new Triangle(coordinatesArray, inputCoordinates);
                return Optional.of(triangle);
            } else {
                System.out.println("Invalid coordinates quantity: " + inputCoordinates.length());
            }
        } else {
            System.out.println("Invalid line. Only numeric values are acceptable: " + inputCoordinates);
        }
        return Optional.empty();
    }

    public static float calculateArea(String[] coordinatesArray) {
        int aX = Integer.parseInt(coordinatesArray[0]);
        int aY = Integer.parseInt(coordinatesArray[1]);

        int bX = Integer.parseInt(coordinatesArray[2]);
        int bY = Integer.parseInt(coordinatesArray[3]);

        int cX = Integer.parseInt(coordinatesArray[4]);
        int cY = Integer.parseInt(coordinatesArray[5]);

        return Math.abs((aX * (bY - cY) + bX * (cY - aY) + cX * (aY - bY))) / 2.0f;
    }

    public static void compareAndUpdate(Triangle triangle, Triangle biggestTriangle) {
        if (triangle.getArea() > biggestTriangle.getArea()) {
            biggestTriangle.setArea(triangle.getArea());
            biggestTriangle.setCoordinates(triangle.getCoordinates());
        }
    }

    //Getters Setters

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }
}
