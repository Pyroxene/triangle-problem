
package com.mycompany.triangleproblem;

import java.io.*;
import java.util.function.Consumer;

@SuppressWarnings("WeakerAccess")
public class IOManager {
    public IOManager(String[] arguments) {
        inputFileName = arguments[0];
        outputFileName = arguments[1];
    }

    private String inputFileName;
    private String outputFileName;

    public void readTheFile(Consumer<String> lineConsumer) {
        File inputFile = new File(inputFileName);
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            String tempLine;
            while ((tempLine = reader.readLine()) != null) {
                lineConsumer.accept(tempLine);
            }
        } catch (IOException e) {
            System.out.println("File is empty");
        }
    }


    public void writeToFile(Triangle outputTriangle) {
        try (FileWriter fileWriter = new FileWriter(outputFileName)) {
            if (outputTriangle.getCoordinates() != null) {
                fileWriter.write(outputTriangle.getCoordinates());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}