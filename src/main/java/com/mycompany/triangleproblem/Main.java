package com.mycompany.triangleproblem;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("No parameters passed. Relaunch with input and output file names entered with .txt extension.");
            System.exit(0);
        }
        Triangle biggestTriangle = new Triangle();
        biggestTriangle.setArea(0);
        IOManager manager = new IOManager(args);
        manager.readTheFile(line -> {
            Optional<Triangle> parsed = Triangle.parse(line);
            parsed.ifPresent(optional -> Triangle.compareAndUpdate(optional, biggestTriangle));


        });
        manager.writeToFile(biggestTriangle);


    }
}

